#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    input: word vector matrix of question,candidate
"""
import numpy as np
import math
def cos_similarity(vector_a,vector_b):
    Len_a = np.sqrt(vector_a.dot(vector_a))+0.00001
    Len_b = np.sqrt(vector_b.dot(vector_b))+0.00001
    np.seterr(divide = 'ignore')
    cos_angle = vector_a.dot(vector_b) /(Len_a*Len_b)
    return cos_angle

def get_similarity_matrix(word_vector_s,word_vector_t):
    similarity_matrix = np.zeros((len(word_vector_s),len(word_vector_t)))
    for i in range(len(word_vector_s)):
        for j in range(len(word_vector_t)):
            similarity_matrix[i][j] = cos_similarity(word_vector_s[i],word_vector_t[j])
    similarity_matrix[np.isnan(similarity_matrix)] = 0.000001
    return similarity_matrix

def match_local_w(similarity_vector,word_vector_matrix,w):
    index = similarity_vector.argmax()
    temp = np.zeros(word_vector_matrix.shape[1])
    temp2 = 0.0001
    for j in range(max(0,index - w) , min(len(word_vector_matrix),index + w)):
        temp =temp + word_vector_matrix[j]*similarity_vector[j]
        temp2 = temp2 + similarity_vector[j]
    result = temp/temp2
    return result
'''
输入：[word_count,200]
'''
def semantic_match(word_vector_s,word_vector_t):
    sim_mat_st = get_similarity_matrix(word_vector_s,word_vector_t)
    sim_mat_ts = get_similarity_matrix(word_vector_t,word_vector_s)
    semantic_match_matrix_st = np.zeros((word_vector_s.shape[0],word_vector_t.shape[1]))
    semantic_match_matrix_ts = np.zeros((word_vector_t.shape[0],word_vector_s.shape[1]))
    for i in range(len(sim_mat_st)):
        semantic_match_matrix_st[i] = match_local_w(sim_mat_st[i],word_vector_t,3)
    for j in range(len(sim_mat_ts)):
        semantic_match_matrix_ts[j] = match_local_w(sim_mat_ts[j],word_vector_s,3)
    return (semantic_match_matrix_st,semantic_match_matrix_ts)
'''
def decompose(word_vec_matrix,sim_matching_matrix):
    word_matrix_plus = np.zeros((word_vec_matrix.shape[0],word_vec_matrix.shape[1]))
    word_matrix_sub = np.zeros((word_vec_matrix.shape[0],word_vec_matrix.shape[1]))
    for i in range(len(word_vec_matrix)):
        weight = cos_similarity(word_vec_matrix[i],sim_matching_matrix[i])
        word_matrix_plus[i] = weight * word_vec_matrix[i]
        word_matrix_sub[i] = (1 - weight) * word_vec_matrix[i]
    result = np.empty((2,word_vec_matrix.shape[0],word_vec_matrix.shape[1]))
    result[0] = word_matrix_plus
    result[1] = word_matrix_sub
    return result
'''
def decompose(word_vec_matrix,sim_matching_matrix):
    word_matrix_plus = np.zeros((word_vec_matrix.shape[0],word_vec_matrix.shape[1]))
    word_matrix_sub = np.zeros((word_vec_matrix.shape[0],word_vec_matrix.shape[1]))
    for i in range(len(word_vec_matrix)):
        weight = np.dot(word_vec_matrix[i],sim_matching_matrix[i])/np.dot(sim_matching_matrix[i],sim_matching_matrix[i])
        word_matrix_plus[i] = weight * sim_matching_matrix[i]
        word_matrix_sub[i] = word_vec_matrix[i] - word_matrix_plus[i]
    result = np.empty((2,word_vec_matrix.shape[0],word_vec_matrix.shape[1]))
    result[0] = word_matrix_plus
    result[1] = word_matrix_sub
    return result
def preprocess(word_vector_s,word_vector_t):
    semantic_match_matrix_st,semantic_match_matrix_ts = semantic_match(word_vector_s,word_vector_t)
    result_s = np.empty((2,50,200),dtype=np.float64)
    result_t = np.empty((2,50,200),dtype=np.float64)
    temp_s = decompose(word_vector_s,semantic_match_matrix_st)
    temp_t = decompose(word_vector_t,semantic_match_matrix_ts)
    # print 't',temp_s.shape,temp_s[0]
    result_s[0][0:temp_s.shape[1]] = temp_s[0]
    result_s[1][0:temp_s.shape[1]] = temp_s[1]
    result_s[0][temp_s.shape[1]:] = 0.00001
    result_s[1][temp_s.shape[1]:] = 0.00001
    result_t[0][0:temp_t.shape[1]] = temp_t[0]
    result_t[1][0:temp_t.shape[1]] = temp_t[1]
    result_t[0][temp_t.shape[1]:] = 0.00001
    result_t[1][temp_t.shape[1]:] = 0.00001
    # print 'r',result_s[0],result_s[1]
    return (result_s,result_t)
'''
if __name__ == "__main__":
    a =  np.array([(1,3,1,2,2),(1,2,1,1,5),(1,4,0,3,1),(1,1,2,1,2),(1,0,1,1,0),(0,2,3,2,0)])
    b = np.array([(1,2,1,1,1),(1,2,3,4,5),(1,2,0,3,1),(1,2,1,1,2),(0,0,0,1,0),(1,2,3,1,0),(0,2,3,2,0)])
    print semantic_match(a,b)[0]
'''
