#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import time
import cPickle
import warnings
import numpy
import theano
import theano.tensor as T
from theano.tensor.signal import downsample
from theano.tensor.nnet import conv
"""
卷积+下采样合成一个层ConvPoolLayer
rng:随机数生成器，用于初始化W
input:4维的向量，theano.tensor.dtensor4
filter_shape:(number of filters, num input feature maps,filter height, filter width)
image_shape:(batch size, num input feature maps,image height, image width)
poolsize: (#rows, #cols)
weight 
bias
"""
class ConvPoolLayer(object):
	def __init__(self, rng, input, filter_shape, input_shape, poolsize=(48, 1),weight = None,bias = None):
		assert input_shape[1] == filter_shape[1]
		self.input = input
		#每个隐层神经元与上一层的连接数为num input feature maps * filter height * filter width。
		#可以用numpy.prod(filter_shape[1:])来求得
		fan_in = numpy.prod(filter_shape[1:])
		#lower layer上每个神经元获得的梯度来自于："num output feature maps * filter height * filter width" /pooling size
		fan_out = (filter_shape[0] * numpy.prod(filter_shape[2:]) /
				   numpy.prod(poolsize))
		if weight is None:
			#以上求得fan_in、fan_out ，将它们代入公式，以此来随机初始化W,W就是线性卷积核
			W_bound = numpy.sqrt(6. / (fan_in + fan_out))
			weight = theano.shared(
				numpy.asarray(
					rng.uniform(low=-W_bound, high=W_bound, size=filter_shape),
					dtype=theano.config.floatX
				),
				borrow=True
			)
		if bias is None:
			# the bias is a 1D tensor -- one bias per output feature map
			#偏置b是一维向量，每个输出图的特征图都对应一个偏置，
			#而输出的特征图的个数由filter个数决定，因此用filter_shape[0]即number of filters来初始化
			b_values = numpy.zeros((filter_shape[0],), dtype=theano.config.floatX)
			bias = theano.shared(value=b_values, borrow=True)
		self.W = weight
		self.b = bias
		#将输入图像与filter卷积，conv.conv2d函数
		#卷积完没有加b再通过sigmoid，这里是一处简化。
		conv_out = conv.conv2d(
			input=input,
			filters=self.W,
			filter_shape=filter_shape,
			image_shape=input_shape
		)
		#加偏置，再通过tanh映射，得到卷积的输出
		#因为b是一维向量，这里用维度转换函数dimshuffle将其reshape。比如b是(10,)，
		#则b.dimshuffle('x', 0, 'x', 'x'))将其reshape为(1,10,1,1)
		pool_input = T.tanh(conv_out + self.b.dimshuffle('x', 0, 'x', 'x'))
		#maxpooling，最大子采样过程
		pooled_out = downsample.max_pool_2d(
			input=pool_input,
			ds=poolsize,
			ignore_border=True
		)
		self.output = pooled_out.reshape((input_shape[0],filter_shape[0]))
		#卷积+采样层的参数
		self.params = [self.W, self.b]
		self.input = input
"""
定义分类层Sigmoid
input_s,input_t (500,1)
n_in = 500
n_out = 1
weight1, weight2, bias
"""
class Sigmoid(object):
	def __init__(self, input_s, input_t, n_in, n_out,weight1=None,weight2 = None,bias= None):
		#W大小是n_in行n_out列，b为n_out维向量。即：每个输出对应W的一列以及b的一个元素。 
		if weight1 is None:
			weight1 = theano.shared(
				value=numpy.zeros(
					(n_in, n_out),
					dtype=theano.config.floatX
				),
				name='W1',
				borrow=True
			)
		if weight2 is None:
			weight2 = theano.shared(
				value=numpy.zeros(
					(n_in, n_out),
					dtype=theano.config.floatX
				),
				name='W2',
				borrow=True
			)
		if bias is None:
			bias = theano.shared(
				value=numpy.zeros(
					(n_out,),
					dtype=theano.config.floatX
				),
				name='b',
				borrow=True
			)
		self.W1 = weight1
		self.W2 = weight2
		self.b = bias
		self.y_given_x = T.nnet.sigmoid(T.dot(input_s, self.W1)+ T.dot(input_t, self.W2) + self.b)
		self.y_pred = self.y_given_x
		#params，LogisticRegression的参数
		self.params = [self.W1,self.W2, self.b]

	def negative_log_likelihood(self, y):
		return -T.mean(y*T.log(self.y_given_x)+(1-y)*T.log(1-self.y_given_x))

	def output(self):
		return self.y_pred

'''
rng 随机数
input1,input2
filter_shape 
input_shape
n_in
conv1_W,conv1_b,conv2_W,conv2_b,sig_W1,sig_W2,sig_b
'''
class Model(object):
	def __init__(self, rng, input1,input2,filter_shape,input_shape, n_in,
				conv1_W = None,conv1_b = None,conv2_W = None,conv2_b =None,
				sig_W1 = None,sig_W2 = None,sig_b =None):
		self.convlayer1 = ConvPoolLayer(
			rng = rng,
			input = input1,
			filter_shape = filter_shape,
			input_shape = input_shape,
			poolsize = (48, 1),
			weight = conv1_W,
			bias = conv1_b
		)
		self.convlayer2 = ConvPoolLayer(
			rng = rng,
			input = input2,
			filter_shape = filter_shape,
			input_shape = input_shape,
			poolsize = (48, 1),
			weight = conv2_W,
			bias = conv2_b
		)
		self.sigmoid = Sigmoid(
			input_s = self.convlayer1.output,
			input_t = self.convlayer2.output,
			n_in = n_in,
			n_out = 1,
			weight1 =sig_W1,
			weight2 = sig_W2,
			bias = sig_b
		)

		#以上已经定义好Model的基本结构，下面是Model模型的其他参数或者函数
		#损失函数Nll（也叫代价函数）
		self.negative_log_likelihood = (
			self.sigmoid.negative_log_likelihood
		)
		#输出
		self.output = self.sigmoid.output
		#Model的参数
		self.params = self.convlayer1.params + self.convlayer2.params + self.sigmoid.params
		# end-snippet-3
		self.predict = theano.function(
			inputs=[input1,input2],
			outputs=self.sigmoid.output(),
		)
	def get_params(self):
		return self.params