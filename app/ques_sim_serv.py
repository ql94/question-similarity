#!/usr/bin/env python
# -*- coding: utf-8 -*-
import ques_cnn
import preprocess
import cPickle
import numpy as np
import theano.tensor as T
from flask import Flask, request, jsonify
import logging
# import time
import codecs

logging.basicConfig(filename='log', level=logging.INFO, format='%(levelname)s - %(threadName)s - %(message)s')

app = Flask(__name__)

# load weight
f = file('params.save',  'rb')
params = cPickle.load(f)
f.close()
# load word_vector
with codecs.open('vectors.txt', encoding='utf-8') as f:
    word_vec_dic = {}
    voc_size, word_vec_size = map(int, f.readline().strip().split(' '))
    parts_len = word_vec_size + 1
    for line_no, line in enumerate(f):
        parts = line.strip().split(' ')
        if len(parts) != parts_len:
            # app.logger.warn('Invalid vector on line %d: %s', line_no + 2, line)
            continue
        word_vec_dic[parts[0]] = np.array(map(np.float64, parts[1:]), dtype=np.float64)

app.logger.info('voc size = %d, word vector size = %d', len(word_vec_dic), word_vec_size)

# 生成一个Model，命名为classifier
index = T.lscalar()
s = T.dtensor4('s')
t = T.dtensor4('t')

input1 = s.reshape((1, 2, 50, word_vec_size))
input2 = t.reshape((1, 2, 50, word_vec_size))
rng = np.random.RandomState(1234)

classifier = ques_cnn.Model(
    rng=rng,
    input1=input1,
    input2=input2,
    filter_shape=(500, 2, 3, word_vec_size),
    input_shape=(1, 2, 50, word_vec_size),
    n_in=500,
    conv1_W=params[0],
    conv1_b=params[1],
    conv2_W=params[2],
    conv2_b=params[3],
    sig_W1=params[4],
    sig_W2=params[5],
    sig_b=params[6],
)


def question_similarity(question, zhidao_title):
    question_feature, candidate_feature = preprocess_sentence(question, zhidao_title)
    '''
    set_s = theano.shared(np.asarray(set_temp_s,
                                dtype=theano.config.floatX),
                                borrow=True)
    set_t = theano.shared(np.asarray(set_temp_t,
                                dtype=theano.config.floatX),
                                borrow=True)
    '''
    similarity = classifier.predict(question_feature, candidate_feature)
    return similarity[0][0]


def preprocess_sentence(question, candidate):
    # 生成词向量
    wordvec_of_question = np.empty((len(question), word_vec_size), dtype=np.float64)
    question_to_remove = []
    for index, word in enumerate(question):
        if word in word_vec_dic:
            wordvec_of_question[index] = word_vec_dic.get(word)  
        else:
            question_to_remove.append(index)
    wordvec_of_question = np.delete(wordvec_of_question,question_to_remove,axis = 0)
    wordvec_of_question = wordvec_of_question[0:50]
    
    wordvec_of_candidate = np.empty((len(candidate), word_vec_size), dtype=np.float64)
    candidate_to_remove = []
    for index, word in enumerate(candidate):
        if word in word_vec_dic:
            wordvec_of_candidate[index] = word_vec_dic.get(word)  
        else:
            candidate_to_remove.append(index)
    wordvec_of_candidate = np.delete(wordvec_of_candidate,candidate_to_remove,axis = 0)        
    wordvec_of_candidate = wordvec_of_candidate[0:50]
    
    # 生成特征矩阵
    question_feature, candidate_feature = preprocess.preprocess(wordvec_of_question, wordvec_of_candidate)
    question_feature = question_feature.reshape((1, 2, 50, word_vec_size))
    candidate_feature = candidate_feature.reshape((1, 2, 50, word_vec_size))
    return (question_feature, candidate_feature)


@app.route('/question-similarity', methods=['POST'])
def handle():
    # start = time.time()
    data = request.get_json(force=True)
    question = data['question']
    sentences = data['sentences']
    similarities = []
    for sentence in sentences:
        try:
            sim = question_similarity(question, sentence)
            # app.logger.info('time = %f, similarities = %s', time.time() - start, similarities)
        except Exception:
            app.logger.warn('Fail to calculate. Param: %s', request.data.decode('utf-8'), exc_info=True)
            sim = 0.0
        similarities.append(sim)
    return jsonify({'scores': similarities})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=False)
