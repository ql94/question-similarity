FROM kaixhin/theano

WORKDIR /opt

RUN pip install flask \
    uwsgi

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y nginx

EXPOSE 80
